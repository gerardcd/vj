#include "Bonus.h"

Bonus::Bonus(int _kind, int _x, int _y, int _w, int _h, int _tex_id)
{
	kind = _kind;  x = _x; y = _y; w = _w; h = _h; tex_id = _tex_id; alive = 1;
}

Bonus::~Bonus()
{
}

void Bonus::Draw(cData& Data)
{
	if (alive > 0) {
		int screen_x, screen_y;

		screen_x = x;
		screen_y = y;
		int xo = 0; int yo = 0; int xf = 1; int yf = 1;
		glEnable(GL_TEXTURE_2D);

		int ID = Data.GetID(tex_id);
		glBindTexture(GL_TEXTURE_2D, ID);
		glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + w, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + w, screen_y + h);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + h);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	}
}

bool Bonus::isAlive()
{
	return alive > 0;
}

void Bonus::kill()
{
	alive = 0;
}

void Bonus::GetArea(cRect *rc)
{
	rc->left = x;
	rc->right = x + w;
	rc->bottom = y;
	rc->top = y + h;
}

int Bonus::GetKind()
{
	return kind;
}