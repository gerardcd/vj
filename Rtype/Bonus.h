#include "cTexture.h"
#include "Globals.h"
#include "cData.h"

#pragma once
class Bonus
{
public:
	Bonus(int kind, int x, int y, int w, int h, int tex_id);
	~Bonus();

	void Draw(cData& Data);
	bool isAlive();
	void kill();
	void GetArea(cRect *rc);
	int GetKind();

private:
	int kind;
	int x;
	int y;
	int w;
	int h;
	int alive;
	int tex_id;
	
};