#include "Enemy.h"
#include "Globals.h"
#include <cstdlib>
#include "cScene.h"

Enemy::Enemy(int _kind, int _x, int _y, int _min, int _max, int _tex_id, int _level)
{
	alive = 1;
	x = _x;
	y = _y;
	kind = _kind;
	orientation = 1;
	max = _max;
	min = _min;
	tex_id = _tex_id;
	switch (kind) {
	case 1:
		w = 50;
		h = 50;
		break;
	case 2:
		w = 30;
		h = 50;
		break;
	case 3:
		w = 65;
		h = 90;
		break;
	case 4:
		w = 120;
		h = 200;
		alive = 7;
		break;
	case 5:
		w = 120;
		h = 200;
		alive = 7;
		break;
	}
	shotDelay = 0;
	shots = std::vector<shot>();
	level = _level;
}


Enemy::~Enemy()
{
}

void Enemy::Draw(cData& Data)
{
	for (int i = 0; i < shots.size(); i++) {
		if (shots[i].isAlive()) shots[i].Draw(Data);
	}
	if (alive > 0) {
		int screen_x, screen_y;

		screen_x = x;
		screen_y = y;
		int xo = 0; int yo = 0; int xf = 1; int yf = 1;
		glEnable(GL_TEXTURE_2D);

		int ID = Data.GetID(tex_id);
		glBindTexture(GL_TEXTURE_2D, ID);
		glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + w, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + w, screen_y + h);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + h);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	}
}

bool Enemy::isAlive() {
	return alive > 0;
}

void Enemy::kill()
{
	if (alive > 0) alive -= 1;
}

void Enemy::KillShot(int i)
{
	shots[i].kill();
}

bool Enemy::CollidesMapWall(int *map, bool right, bool up)
{
	int tile_x, tile_y;
	int i, j;
	int width_tiles, height_tiles;

	int n = 0;
	for (i = 0; i < h; i++)
		for (j = 0; j < w; j++) {
			{
				tile_x = (x + j) / TILE_SIZE;
				tile_y = (y + i) / TILE_SIZE;
				if (
					map[tile_x + (tile_y*SCENE_WIDTH)] != 0 /*&&
															!(up && (j == w-1 || j == 0)) &&
															!(right && (i == h-1 || i == 0))*/
					)
					n++;
			}
		}
	return n > 0;
}

void Enemy::Logic(int *map, int px, int py)
{
	if (alive) {
		if (kind == 1 || kind == 4 || kind == 5) {
			if (orientation == 1) {
				if (y < max) {
					y++;
				}
				else orientation = -1;
			}
			else {
				if (y > min) {
					y--;
				}
				else orientation = 1;
			}
		}
		else if (kind == 2) {
			if (orientation == 1) {
				if (x < max) {
					x++;
				}
				else orientation = -1;
			}
			else {
				if (x > min) {
					x--;
				}
				else orientation = 1;
			}
		}
		else if (kind == 3) {
			int xaux = x; int yaux = y;
			if (x < px + 500 && x < max) x++;

			bool collides = CollidesMapWall(map, true, false);
			if (collides) x = xaux;

			if (y < py) y++; 
			if (y > py) y--;

			collides = CollidesMapWall(map, true, false);
			if (collides) y = yaux;
		}

		if (shotDelay == 0) {
			makeShot(px, py);
			shotDelay = 10 + (rand() % (90 / level));
		}
		if (shotDelay > 0) shotDelay--;
	}

	for (int i = 0; i < shots.size(); i++) {
		shots[i].move(map);
	}
}

void Enemy::GetArea(cRect *rc)
{
	rc->left = x;
	rc->right = x + w;
	rc->bottom = y;
	rc->top = y + h;
}

int Enemy::GetTexId()
{
	return tex_id;
}

void Enemy::makeShot(int px, int py)
{
	if (kind == 1 || kind == 2 || kind == 3) {
		int vx = px - x;
		int vy = py - y;
		float mod = sqrtf(abs(vx) ^ 2 + abs(vy) ^ 2);
		if (mod < 20) {
			float wx = min(2, ((float)vx) / abs(mod)) + MAP_SPEED;
			if (wx < 0) wx /= 4.0;
			float wy = min(2, ((float)vy) / abs(mod));
			shot s(x + (w / 2), y + (h / 2), 10, 10, wx, wy, IMG_ENEMY_SHOT);
			shots.push_back(s);
		}
	}
	if (kind == 4) {
		shot s = shot(x, y + h -20, 10, 10, -2, -((rand() % 1000) / 500), IMG_ENEMY_SHOT);
		shots.push_back(s);
		s = shot(x, y +20, 10, 10, -2, ((rand() % 1000) / 500), IMG_ENEMY_SHOT);
		shots.push_back(s);
		s = shot(x, y + (h / 2), 10, 10, -2, ((rand() % 1000) / 500) - 1, IMG_ENEMY_SHOT);
		shots.push_back(s);
		int rnd = rand() % 1000;
		int sx = x - rnd;
		s = shot(sx, 380, 15, 30, 0, -4, IMG_BOSS_SHOT1);
		shots.push_back(s);
		if (rnd < 500){
			s = shot(x - 11, y, 10, h, -4, 0, IMG_BOSS_SHOT2);
			shots.push_back(s);
		}
	}
	if (kind == 5) {
		shot s = shot(x-50, y + h+50, 10, 10, -2, 0, IMG_ENEMY_SHOT, true);
		shots.push_back(s);
		s = shot(x, y + 20, 10, 10, -2, 0, IMG_ENEMY_SHOT, true);
		shots.push_back(s);
		int rnd = rand() % 1000;
		if (rnd < 500) {
			s = shot(x - 550, y, 10, h, 1, 0, IMG_BOSS_SHOT2);
			shots.push_back(s);
		}
	}
}

std::vector<shot> Enemy::getShots()
{
	return shots;
}
