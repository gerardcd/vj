#include "cTexture.h"
#include "Globals.h"
#include "shot.h"
#include "cData.h"

#pragma once

class Enemy
{
public:
	Enemy(int kind, int x, int y, int min, int max, int tex_id, int level);
	~Enemy();

	void Draw(cData& Data);
	bool isAlive();
	void kill();
	void KillShot(int i);
	bool CollidesMapWall(int * map, bool right, bool up);
	void Logic(int *map, int x, int y);
	void GetArea(cRect *rc);
	int GetTexId();
	void makeShot(int x, int y);
	std::vector<shot> getShots();
private:
	int kind;
	int x;
	int y;
	int w;
	int h;
	int max;
	int min;
	int alive;
	int direction;
	int orientation;
	int tex_id;
	std::vector<shot> shots;
	int shotDelay;
	int level;

};

