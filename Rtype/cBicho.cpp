#include "cBicho.h"
#include "cScene.h"
#include "Globals.h"

cBicho::cBicho(void)
{
	Init();
}
cBicho::~cBicho(void){}

cBicho::cBicho(int posx,int posy,int width,int height)
{
	x = posx;
	y = posy;
	w = width;
	h = height;
}
void cBicho::Init()
{
	shots = std::vector<shot>();
	delay = 0;
	shotDelay = 0;
	shield = 0;
	doubleShot = 0;
}

void cBicho::SetPosition(int posx,int posy)
{
	x = posx;
	y = posy;
}

void cBicho::GetPosition(int *posx,int *posy)
{
	*posx = x;
	*posy = y;
}

void cBicho::SetTile(int tx,int ty)
{
	x = tx * TILE_SIZE;
	y = ty * TILE_SIZE;
}

void cBicho::GetTile(int *tx,int *ty)
{
	*tx = x / TILE_SIZE;
	*ty = y / TILE_SIZE;
}
void cBicho::SetWidthHeight(int width,int height)
{
	w = width;
	h = height;
}
void cBicho::GetWidthHeight(int *width,int *height)
{
	*width = w;
	*height = h;
}
bool cBicho::Collides(cRect *rc)
{
	return ((x+w>rc->left) && (x<rc->right) && (y+h>rc->bottom) && (y<rc->top));
}
bool cBicho::CollidesMapWall(int *map,bool right, bool up)
{
	int tile_x,tile_y;
	int i, j;
	int width_tiles,height_tiles;

	int n = 0;
	for (i = 0; i < h; i++)
		for (j = 0; j < w; j++) {
		{
			tile_x = (x + j) / TILE_SIZE;
			tile_y = (y + i) / TILE_SIZE;
			if (
				map[tile_x + (tile_y*SCENE_WIDTH)] != 0 /*&& 
				!(up && (j == w-1 || j == 0)) && 
				!(right && (i == h-1 || i == 0))*/
			)
				n++;
		}
	}
	return n > 0;
}

bool cBicho::CollidesMapFloor(int *map)
{
	int tile_x,tile_y;
	int width_tiles;
	bool on_base;
	int i;

	tile_x = x / TILE_SIZE;
	tile_y = y / TILE_SIZE;

	width_tiles = w / TILE_SIZE;
	if( (x % TILE_SIZE) != 0) width_tiles++;

	on_base = false;
	i=0;
	while((i<width_tiles) && !on_base)
	{
		if( (y % TILE_SIZE) == 0 )
		{
			if(map[ (tile_x + i) + ((tile_y - 1) * SCENE_WIDTH) ] != 0)
				on_base = true;
		}
		else
		{
			if(map[ (tile_x + i) + (tile_y * SCENE_WIDTH) ] != 0)
			{
				y = (tile_y + 1) * TILE_SIZE;
				on_base = true;
			}
		}
		i++;
	}
	return on_base;
}

void cBicho::GetArea(cRect *rc)
{
	rc->left   = x;
	rc->right  = x+w;
	rc->bottom = y;
	rc->top    = y+h;
}
void cBicho::DrawRect(cData& Data,float xo,float yo,float xf,float yf)
{
	int screen_x,screen_y;

	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	if (shield) {
		xo = 0.4f;
		xf = 0.9;
	}
	
	glEnable(GL_TEXTURE_2D);
	
	int ID = Data.GetID(IMG_PLAYER);
	glBindTexture(GL_TEXTURE_2D, ID);
	glBegin(GL_QUADS);	
		glTexCoord2f(xo,yo);	glVertex2i(screen_x  ,screen_y);
		glTexCoord2f(xf,yo);	glVertex2i(screen_x+w,screen_y);
		glTexCoord2f(xf,yf);	glVertex2i(screen_x+w,screen_y+h);
		glTexCoord2f(xo,yf);	glVertex2i(screen_x  ,screen_y+h);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	for (int i = 0; i < shots.size(); i++) {
		if (shots[i].isAlive()) shots[i].Draw(Data);
	}
}

void cBicho::MoveLeft(int *map)
{
	int xaux;
	
	//Whats next tile?
	if( (x % TILE_SIZE) == 0)
	{
		xaux = x;
		x -= STEP_LENGTH * 2;

		if(CollidesMapWall(map, true, false))
		{
			x = xaux;
			state = STATE_LOOKLEFT;
		}
	}
	//Advance, no problem
	else
	{
		x -= (x > LEVEL_LENGTH ? STEP_LENGTH : STEP_LENGTH * 2 );
		if(state != STATE_WALKLEFT)
		{
			state = STATE_WALKLEFT;
			seq = 0;
			delay = 0;
		}
	}
}
void cBicho::MoveRight(int *map)
{
	int xaux;

	//Whats next tile?
	if( (x % TILE_SIZE) == 0)
	{
		xaux = x;
		x += STEP_LENGTH;

		if(CollidesMapWall(map,true, false))
		{
			x = xaux;
			state = STATE_LOOKRIGHT;
		}
	}
	//Advance, no problem
	else
	{
		x += STEP_LENGTH;

		if(state != STATE_WALKRIGHT)
		{
			state = STATE_WALKRIGHT;
			seq = 0;
			delay = 0;
		}
	}
}
void cBicho::Stop()
{
	switch(state)
	{
		case STATE_WALKLEFT:	state = STATE_LOOKLEFT;		break;
		case STATE_WALKRIGHT:	state = STATE_LOOKRIGHT;	break;
	}
}

void cBicho::MoveUp(int *map)
{
	int yaux;
	
	//Whats next tile?
	if( (y % TILE_SIZE) == 0)
	{
		yaux = y;
		y += STEP_LENGTH;

		if(CollidesMapWall(map, false, true))
		{
			y = yaux;
		}
	}
	//Advance, no problem
	else
	{
		y += STEP_LENGTH;
	}
}

void cBicho::MoveDown(int *map)
{
	int yaux;
	
	//Whats next tile?
	if( (y % TILE_SIZE) == 0)
	{
		yaux = y;
		y -= STEP_LENGTH;

		if(CollidesMapWall(map,false, true))
		{
			y = yaux;
		}
	}
	//Advance, no problem
	else
	{
		y -= STEP_LENGTH;
	}
}

void cBicho::NaturalFlight(int * map)
{
	if (x < LEVEL_LENGTH) {
		int xaux;

		//Whats next tile?
		if ((x % TILE_SIZE) == 0)
		{
			xaux = x;
			x += MAP_SPEED;

			if (CollidesMapWall(map, true, false)) x = xaux;
		}
		//Advance, no problem
		else
		{
			x += STEP_LENGTH;
			/*
			if (state != STATE_WALKRIGHT)
			{
				state = STATE_WALKRIGHT;
				seq = 0;
				delay = 0;
			}*/
		}
	}
}

//return: 0 alive; 1 killed; 2 level passed

int cBicho::Logic(int *map, std::vector<Enemy>& Enemies, std::vector<Bonus>& Bonuses)
{
	int ret = 0;
	cRect rc;
	//colision disparos player con enemigos
	for (int i = 0; i < shots.size(); i++) {
		shots[i].move(map);
		for (int j = 0; j < Enemies.size(); j++) {
			Enemies[j].GetArea(&rc);
			if (Enemies[j].isAlive() && shots[i].isAlive() && shots[i].Collides(&rc)) {
				Enemies[j].kill();
				shots[i].kill();
				if (j == Enemies.size() - 1 && !Enemies[j].isAlive()) 
					ret = 2;
			}
		}
	}

	//colisiones enemigos
	std::vector<shot> EShots;
	for (int j = 0; j < Enemies.size(); j++) {
		//colision player enemigos
		Enemies[j].GetArea(&rc);
		if (Enemies[j].isAlive() && Collides(&rc)) {
			ret = 1;
		}
		//colision player con proyectiles enemigos
		EShots = Enemies[j].getShots();
		GetArea(&rc);
		for (int i = 0; i < EShots.size(); i++) {
			if (EShots[i].isAlive() && EShots[i].Collides(&rc)) {
				ret = 1;
				Enemies[j].KillShot(i);
			}
		}
	}

	//colisiones bonuses
	for (int j = 0; j < Bonuses.size(); j++) {
		Bonuses[j].GetArea(&rc);
		if (Bonuses[j].isAlive() && Collides(&rc)) {
			Bonuses[j].kill();
			if (Bonuses[j].GetKind() == 1) {
				shield = true;
				SetWidthHeight(70, 20);
			}
			else {
				doubleShot = true;
			}
		}
	}

	if (ret == 1 && shield) {
		shield = false;
		SetWidthHeight(50, 20);
		ret = 0;
	}

	if (shotDelay > 0) shotDelay--;
	return ret;
}

void cBicho::NextFrame(int max)
{
	delay++;
	if(delay == FRAME_DELAY)
	{
		seq++;
		seq%=max;
		delay = 0;
	}
}
int cBicho::GetFrame()
{
	return seq;
}
void cBicho::makeShot()
{
	if (shotDelay == 0) {

		if (doubleShot){
			shot s(x + w, y + h, 50, 10, 8, 0, IMG_PLAYER_SHOT);
			shots.push_back(s);
			s = shot(x + w, y, 50, 10, 8, 0, IMG_PLAYER_SHOT);
			shots.push_back(s);
		}
		else {
			shot s(x + w, y + (h / 2), 50, 10, 8, 0, IMG_PLAYER_SHOT);
			shots.push_back(s);
		}
		shotDelay = 50;
	}
}
void cBicho::setShield()
{
	shield = true;
}
void cBicho::setDouble()
{
	doubleShot = true;
}
int cBicho::GetState()
{
	return state;
}
void cBicho::SetState(int s)
{
	state = s;
}