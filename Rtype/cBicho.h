#pragma once

#include "cTexture.h"
#include "Globals.h"
#include "Enemy.h"
#include "shot.h"
#include "Bonus.h"

#define FRAME_DELAY		8
#define STEP_LENGTH		2
#define JUMP_HEIGHT		96
#define JUMP_STEP		4

#define STATE_LOOKLEFT		0
#define STATE_LOOKRIGHT		1
#define STATE_WALKLEFT		2
#define STATE_WALKRIGHT		3


class cBicho
{
public:
	cBicho(void);
	cBicho(int x,int y,int w,int h);
	~cBicho(void);

	void Init();
	void SetPosition(int x,int y);
	void GetPosition(int *x,int *y);
	void SetTile(int tx,int ty);
	void GetTile(int *tx,int *ty);
	void SetWidthHeight(int w,int h);
	void GetWidthHeight(int *w,int *h);

	bool Collides(cRect *rc);
	bool CollidesMapWall(int *map,bool right, bool up);
	bool CollidesMapFloor(int *map);
	void GetArea(cRect *rc);
	void DrawRect(cData& Data,float xo,float yo,float xf,float yf);

	void MoveRight(int *map);
	void MoveLeft(int *map);
	void MoveUp(int *map);
	void MoveDown(int *map);
	void NaturalFlight(int *map);
	void Stop();

	int  GetState();
	void SetState(int s);

	int Logic(int * map, std::vector<Enemy>& enemies, std::vector<Bonus>& Bonuses);

	void NextFrame(int max);
	int  GetFrame();

	void makeShot();
	void setShield();
	void setDouble();
	
private:
	int x,y;
	int w,h;
	int state;

	bool jumping;
	int jump_alfa;
	int jump_y;

	int seq,delay, shotDelay;
	
	bool shield;
	bool doubleShot;

	std::vector<shot> shots;
};
