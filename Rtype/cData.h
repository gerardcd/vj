#pragma once

#include "cTexture.h"
#include "Globals.h"

//Image array size
#define NUM_IMG		21

//Image identifiers
#define IMG_BLOCKS	0
#define IMG_PLAYER	1
#define IMG_SHOT	2
#define IMG_ENEMY1	3
#define IMG_ENEMY2	4
#define IMG_ENEMY3	5
#define IMG_BOSS1	6
#define IMG_ENEMY_SHOT	7
#define IMG_BOSS_SHOT1	8
#define IMG_BOSS_SHOT2	9
#define IMG_PLAYER_SHOT 10
#define BACKGROUND_IMG	11
#define MENU_IMG		12
#define WON_IMG			13
#define CREDITS_IMG		14
#define IMG_BONUS1		15
#define IMG_BONUS2		16
#define IMG_BOSS2		17
#define MENU_KILLED		18
#define MENU_CREDITS		19
#define MENU_WON			20


class cData
{
public:
	cData(void);
	~cData(void);

	int  GetID(int img);
	void GetSize(int img,int *w,int *h);
	bool LoadImage(int img,char *filename,int type = GL_RGBA);

private:
	cTexture texture[NUM_IMG];
};
