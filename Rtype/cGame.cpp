#include "cGame.h"
#include "Globals.h"
#include <time.h>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

cGame::cGame(void)
{
}

cGame::~cGame(void)
{
}

bool cGame::Init(int _level)
{
	bool res=true;
	//Graphics initialization
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,GAME_WIDTH,0,GAME_HEIGHT,0,1);
	glMatrixMode(GL_MODELVIEW);
	
	glAlphaFunc(GL_GREATER, 0.05f);
	glEnable(GL_ALPHA_TEST);

	//Scene initialization
	res = Data.LoadImage(IMG_BLOCKS, "blocks.png", GL_RGBA);
	res = Data.LoadImage(BACKGROUND_IMG, "fons.png", GL_RGBA);
	res = Data.LoadImage(MENU_IMG, "menu.png", GL_RGBA);
	res = Data.LoadImage(MENU_KILLED, "killed.png", GL_RGBA);
	res = Data.LoadImage(MENU_CREDITS, "credits.png", GL_RGBA);
	res = Data.LoadImage(MENU_WON, "won.png",GL_RGBA);

	//Player initialization
	res = Data.LoadImage(IMG_PLAYER,"nau.png",GL_RGBA);
	res = Data.LoadImage(IMG_PLAYER_SHOT, "player_shot.png", GL_RGBA);
	res = Data.LoadImage(IMG_ENEMY_SHOT, "enemy_shot.png", GL_RGBA);
	res = Data.LoadImage(IMG_BOSS_SHOT1, "boss_shot1.png", GL_RGBA);
	res = Data.LoadImage(IMG_BOSS_SHOT2, "boss_shot1.png", GL_RGBA);
	if(!res) return false;
	Player.Init();
	Player.SetWidthHeight(50,20);
	Player.SetTile(10,20);
	Player.SetState(STATE_LOOKRIGHT);

	res = Data.LoadImage(IMG_ENEMY1, "enemy1.png", GL_RGBA);
	res = Data.LoadImage(IMG_ENEMY2, "enemy2.png", GL_RGBA);
	res = Data.LoadImage(IMG_ENEMY3, "enemy3.png", GL_RGBA);
	res = Data.LoadImage(IMG_BOSS1, "boss1.png", GL_RGBA);
	res = Data.LoadImage(IMG_BOSS2, "boss2.png", GL_RGBA);
	res = Data.LoadImage(IMG_BONUS1, "bonus1.png", GL_RGBA);
	res = Data.LoadImage(IMG_BONUS2, "bonus2.png", GL_RGBA);

	level = _level;

	//Enemies initialization
	Enemies = std::vector<Enemy>();
	Bonuses = std::vector<Bonus>();

	if (!res) return false;
	res = Scene.LoadLevel(level);
	if (!res) return false;

	if (level == 1) {
		//Bonus(kind, x, y, w, h, tex_id)
		Bonus bon = Bonus(1, 1800, 150, 26, 40, IMG_BONUS1); // 13 20
		Bonuses.push_back(bon);
		bon = Bonus(2, 1550, 150, 60, 40, IMG_BONUS2); // 13 20
		Bonuses.push_back(bon);

		//Enemy(kind, x, y, min, max, texture); kind (move): 1 = y, 2 = x, 3 = follow
		Enemy enemy = Enemy(1, 900, 200, 100, 300, IMG_ENEMY1,1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 1200, 150, 1000, 1300, IMG_ENEMY2,1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 900, 150, 600, 900, IMG_ENEMY2,1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 1300, 300, 1000, 1500, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);

		enemy = Enemy(3, 1900, 150, 0, 2500, IMG_ENEMY3,1);
		Enemies.push_back(enemy);

		enemy = Enemy(2, 2999, 350, 2500, 3000, IMG_ENEMY2,1);
		Enemies.push_back(enemy);
		
		enemy = Enemy(1, 3100, 299, 100, 300, IMG_ENEMY1,1);
		Enemies.push_back(enemy);
		enemy = Enemy(1, 3300, 101, 100, 300, IMG_ENEMY1,1);
		Enemies.push_back(enemy);
		enemy = Enemy(1, 3500, 299, 100, 300, IMG_ENEMY1,1);
		Enemies.push_back(enemy);
		enemy = Enemy(1, 3700, 101, 100, 300, IMG_ENEMY1,1);
		Enemies.push_back(enemy);

		enemy = Enemy(4, 5000, 200, 120, 170, IMG_BOSS1,1);
		Enemies.push_back(enemy);

	}
	if (level == 2) {
		//Enemy(kind, x, y, min, max, texture); 1 = y, 2 = x, 3 = follow
		Bonus bon = Bonus(1, 800, 350, 26, 40, IMG_BONUS1); // 13 20
		Bonuses.push_back(bon);
		Enemy enemy = Enemy(1, 900, 200, 200, 270, IMG_ENEMY1, 1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 1000, 200, 1000, 1300, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);

		enemy = Enemy(3, 1500, 90, 0, 2500, IMG_ENEMY3, 1);
		Enemies.push_back(enemy); // drac tancat

		enemy = Enemy(1, 2190, 101, 100, 350, IMG_ENEMY1, 1);
		Enemies.push_back(enemy); // 2 columnes

		bon = Bonus(2, 2190, 100, 60, 40, IMG_BONUS2); // 13 20
		Bonuses.push_back(bon);


		//bateria enemics x
		enemy = Enemy(2, 4100, 350, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 4100, 250, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 4100, 150, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);

		enemy = Enemy(2, 4200, 275, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 4200, 175, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);

		enemy = Enemy(2, 4300, 350, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 4300, 250, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);
		enemy = Enemy(2, 4300, 150, 2100, 3000, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);

		//forat abans boss
		enemy = Enemy(1, 3750, 300, 170, 360, IMG_ENEMY2, 1);
		Enemies.push_back(enemy);

		bon = Bonus(1, 3765, 280, 26, 40, IMG_BONUS1); // 13 20
		Bonuses.push_back(bon);

		enemy = Enemy(5, 5000, 200, 125, 125, IMG_BOSS2, 1);
		Enemies.push_back(enemy);
	}

	pause = true;
	n = 0;
	killed = false;
	won = false;

	return res;
}

bool cGame::Loop()
{
	int startTime = glutGet(GLUT_ELAPSED_TIME);
	bool res = true;

	if (keys[GLUT_KEY_F1] && n == 10 && !killed) {
		pause = !pause;
		n = 0;
	}
	if (n < 10) n++;

	if (keys[27])	res = false;

	if (!pause && !killed && !won) {
		res = Process();
		if (res) Render();
	}
	else {
		if (won) {
			if (level < 2) {
				WonRender();
				if (keys[GLUT_KEY_F1]) {
					level += 1;
					Init(level);
					pause = false;
				}
			}
			else CreditsRender();
		}
		if (pause) {
			MenuRender();
			if (keys[GLUT_KEY_F2]) {
				Init(level);
				pause = false;
			}
		}
		if (killed) {
			KilledRender();
			if (keys[GLUT_KEY_F2]) {
				Init(level);
				pause = false;
			}
		}
	}

	int timePerFrame = 20;
	int endTime = glutGet(GLUT_ELAPSED_TIME);
	int loopTime = endTime - startTime;
	int sleepTime = timePerFrame - loopTime;
	if (sleepTime > 0) std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));

	return res;
}

void cGame::Finalize()
{
}

//Input
void cGame::ReadKeyboard(unsigned char key, int x, int y, bool press)
{
	keys[key] = press;
}

void cGame::ReadMouse(int button, int state, int x, int y)
{
}

//Process
bool cGame::Process()
{
	bool res=true;
	
	//Process Input

	Player.NaturalFlight(Scene.GetMap());

	if(keys[GLUT_KEY_UP])			Player.MoveUp(Scene.GetMap());
	if(keys[GLUT_KEY_DOWN])			Player.MoveDown(Scene.GetMap());
	if(keys[GLUT_KEY_LEFT])			Player.MoveLeft(Scene.GetMap());
	if (keys[GLUT_KEY_RIGHT])		Player.MoveRight(Scene.GetMap());
	if(keys[GLUT_KEY_F3])			Player.makeShot();
	else Player.Stop();
	

	//Game Logic
	int px, py;
	Player.GetPosition(&px, &py);
	for (int i = 0; i < Enemies.size(); i++) {
		Enemies[i].Logic(Scene.GetMap(), px, py);
	}
	int state = Player.Logic(Scene.GetMap(), Enemies, Bonuses);
	if (state == 1) killed = true;
	if (state == 2) won = true;
	
	if (px < - Scene.GetX() -100) killed = true;
	killed = false;

	return res;
}

//Output
void cGame::Render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	
	glLoadIdentity();
	int ID = Data.GetID(IMG_BLOCKS);
	Scene.Draw(Data.GetID(IMG_BLOCKS), Data.GetID(BACKGROUND_IMG));
	for (int i = 0; i < Bonuses.size(); i++) {
		Bonuses[i].Draw(Data);
	}
	Player.Draw(Data);
	for (int i = 0; i < Enemies.size(); i++) {
		Enemies[i].Draw(Data);
	}
	
	glutSwapBuffers();
}

void cGame::MenuRender()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Data.GetID(MENU_IMG));
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);	glVertex2i(0, 0);
	glTexCoord2f(1, 0);	glVertex2i(4 * GAME_WIDTH, 0);
	glTexCoord2f(1, 1);	glVertex2i(4 * GAME_WIDTH, GAME_HEIGHT);
	glTexCoord2f(0, 1);	glVertex2i(0, GAME_HEIGHT);
	glEnd();

	glutSwapBuffers();
}

void cGame::WonRender()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Data.GetID(MENU_WON));
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);	glVertex2i(0, 0);
	glTexCoord2f(1, 0);	glVertex2i(4 * GAME_WIDTH, 0);
	glTexCoord2f(1, 1);	glVertex2i(4 * GAME_WIDTH, GAME_HEIGHT);
	glTexCoord2f(0, 1);	glVertex2i(0, GAME_HEIGHT);
	glEnd();

	glutSwapBuffers();
}

void cGame::CreditsRender()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Data.GetID(MENU_CREDITS));
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);	glVertex2i(0, 0);
	glTexCoord2f(1, 0);	glVertex2i(4 * GAME_WIDTH, 0);
	glTexCoord2f(1, 1);	glVertex2i(4 * GAME_WIDTH, GAME_HEIGHT);
	glTexCoord2f(0, 1);	glVertex2i(0, GAME_HEIGHT);
	glEnd();

	glutSwapBuffers();
}

void cGame::KilledRender()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Data.GetID(MENU_KILLED));
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);	glVertex2i(0, 0);
	glTexCoord2f(1, 0);	glVertex2i(4 * GAME_WIDTH, 0);
	glTexCoord2f(1, 1);	glVertex2i(4 * GAME_WIDTH, GAME_HEIGHT);
	glTexCoord2f(0, 1);	glVertex2i(0, GAME_HEIGHT);
	glEnd();

	glutSwapBuffers();
}
