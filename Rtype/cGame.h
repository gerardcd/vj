#pragma once

#include "cScene.h"
#include "cPlayer.h"
#include "Enemy.h"
#include "Bonus.h"
#include "cData.h"

class cGame
{
public:
	cGame(void);
	virtual ~cGame(void);

	bool Init(int level);
	bool Loop();
	void Finalize();

	//Input
	void ReadKeyboard(unsigned char key, int x, int y, bool press);
	void ReadMouse(int button, int state, int x, int y);
	//Process
	bool Process();
	//Output
	void Render();
	void MenuRender();
	void WonRender();
	void CreditsRender();
	void KilledRender();

private:
	unsigned char keys[256];
	cScene Scene;
	cPlayer Player;
	std::vector<Enemy> Enemies;
	std::vector<Bonus> Bonuses;
	cData Data;
	bool pause;
	int n;
	bool killed;
	int level;
	bool won;
};
