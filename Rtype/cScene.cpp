#include "cScene.h"
#include "Globals.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

cScene::cScene(void)
{
}

cScene::~cScene(void)
{
}

bool cScene::LoadLevel(int level)
{
	bool res;
	FILE *fd;
	char file[16];
	int i,j,px,py;
	float coordx_tile, coordy_tile;

	res=true;

	if(level<10) sprintf(file,"%s0%d%s",(char *)FILENAME,level,(char *)FILENAME_EXT);
	else		 sprintf(file,"%s%d%s",(char *)FILENAME,level,(char *)FILENAME_EXT);

	fd=fopen(file,"r");
	if(fd==NULL) return false;

	id_DL=glGenLists(1);
	glNewList(id_DL,GL_COMPILE);
		glBegin(GL_QUADS);
			std::ifstream  data(file);

			i = 0;
			std::string line;
			while (std::getline(data, line))
			{
				j = 0;
				std::stringstream  lineStream(line);
				std::string        tile;
				while (std::getline(lineStream, tile, ','))
				{
					px = SCENE_Xo + (j*TILE_SIZE);
					py = (SCENE_Yo + (i*TILE_SIZE));

					if (tile == "-1")
					{
						//Tiles must be != 0 !!!
						map[(i*SCENE_WIDTH) + j] = 0;
					}
					else
					{
						//Tiles = 1,2,3,...
						map[(i*SCENE_WIDTH) + j] = 1;

						if (tile == "0")	coordx_tile = 0.0f;
						if (tile == "36")	coordx_tile = 0.0f;
						if (tile == "19")	coordx_tile = 0.5f;
						if (tile == "21")	coordx_tile = 0.5f;

						if (tile == "0")	coordy_tile = 0.0f;
						if (tile == "36")	coordy_tile = 0.25f;
						if (tile == "19")	coordy_tile = 0.0f;
						if (tile == "21")	coordy_tile = 0.25f;
						
						//BLOCK_SIZE = 24, FILE_SIZE = 64
						float offsetx = 0.5f; float offsety = 0.25;
						glTexCoord2f(coordx_tile, coordy_tile + offsety);			glVertex2i(px, py);
						glTexCoord2f(coordx_tile + offsetx, coordy_tile + offsety);	glVertex2i(px + BLOCK_SIZE, py);
						glTexCoord2f(coordx_tile + offsetx, coordy_tile);			glVertex2i(px + BLOCK_SIZE, py + BLOCK_SIZE);
						glTexCoord2f(coordx_tile, coordy_tile);						glVertex2i(px, py + BLOCK_SIZE);
					}
					j++;
				}
				i++;
			}
		glEnd();
	glEndList();

	fclose(fd);
	x = 0;
	return res;
}

void cScene::Draw(int tex_id, int back_id)
{

	if (x > -LEVEL_LENGTH) x -= MAP_SPEED;
	glTranslated(x, 0, 0);

	//fons
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, back_id);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);	glVertex2i(0, 0);
	glTexCoord2f(1, 0);	glVertex2i(4*GAME_WIDTH, 0);
	glTexCoord2f(1, 1);	glVertex2i(4*GAME_WIDTH, GAME_HEIGHT);
	glTexCoord2f(0, 1);	glVertex2i(0, GAME_HEIGHT);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	//mapa
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glCallList(id_DL);

	glDisable(GL_TEXTURE_2D);


}
int* cScene::GetMap()
{
	return map;
}

int cScene::GetX()
{
	return x;
}

