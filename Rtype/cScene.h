#pragma once

#include "cTexture.h"

#define SCENE_Xo		0
#define SCENE_Yo		0
#define SCENE_WIDTH		500
#define SCENE_HEIGHT	50

#define FILENAME		"level"
#define FILENAME_EXT	".csv"

#define TILE_SIZE		10
#define BLOCK_SIZE		10

#define MAP_SPEED		2

class cScene
{
public:
	cScene(void);
	virtual ~cScene(void);

	bool LoadLevel(int level);
	void Draw(int tex_id, int back_id);
	int *GetMap();
	int GetX();

private:
	int map[SCENE_WIDTH * SCENE_HEIGHT];	//scene
	int id_DL;								//actual level display list
	int x;
};
