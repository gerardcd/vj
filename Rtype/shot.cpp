#include "shot.h"
#include "Globals.h"
#include "cScene.h"
#include <cstdlib>


shot::shot(float _x, float _y, int _w, int _h, float _vx, float _vy, int _tex_id)
{
	x = _x;
	y = _y;
	w = _w;
	h = _h;
	vx = _vx;
	vy = _vy;
	tex_id = _tex_id;
	alive = true;
	randomize = false;
}

shot::shot(float _x, float _y, int _w, int _h, float _vx, float _vy, int _tex_id, bool _randomize)
{
	x = _x;
	y = _y;
	w = _w;
	h = _h;
	vx = _vx;
	vy = _vy;
	tex_id = _tex_id;
	alive = true;
	randomize = true;
}


shot::~shot()
{
}

bool shot::Collides(cRect * rc)
{
	return ((x + w>rc->left) && (x<rc->right) && (y + h>rc->bottom) && (y<rc->top));
}

bool shot::CollidesMapWall(int *map)
{
	int tile_x, tile_y;
	int i, j;
	int width_tiles, height_tiles;
	int access;
	int n = 0;
	for (i = 0; i < h; i++)
		for (j = 0; j < w; j++) {
			{
				tile_x = (x + j) / TILE_SIZE;
				tile_y = (y + i) / TILE_SIZE;
				access = tile_x + (tile_y*SCENE_WIDTH);
				if (
					access >= SCENE_HEIGHT*SCENE_WIDTH ||
					access < 0 ||
					map[access] != 0
					)
					return true;
			}
		}
	return false;
}

void shot::move(int * map)
{
	if ((randomize) && ((rand() % 100) < 10)) vy = float ((rand() % 20) - 10.0) / 5.0;

	x += vx;
	y += vy;
	
	if (CollidesMapWall(map)) kill();
}

void shot::kill() {
	alive = false;
}

void shot::Draw(cData& Data)
{
	int screen_x, screen_y;

	screen_x = x;
	screen_y = y;
	int xo = 0; int yo = 0; int xf = 1; int yf = 1;
	glEnable(GL_TEXTURE_2D);

	int ID = Data.GetID(tex_id);
	if (tex_id == 10) 
		ID = ID;
	glBindTexture(GL_TEXTURE_2D, ID);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + w, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + w, screen_y + h);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + h);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

bool shot::isAlive()
{
	return alive;
}

