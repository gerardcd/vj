#pragma once
#include "Globals.h"
#include "cData.h"
class shot
{
public:
	shot(float x, float y, int _w, int _h, float vx, float vy, int tex_id);
	shot(float x, float y, int _w, int _h, float vx, float vy, int tex_id, bool randomize);
	~shot();
	bool Collides(cRect *rc);
	bool CollidesMapWall(int *map);
	void move(int *map);
	void kill();
	void Draw(cData& Data);
	bool isAlive();

private:
	float x;
	float y;
	int w;
	int h;
	float vx;
	float vy;
	int tex_id;
	bool alive;
	bool randomize;
};

